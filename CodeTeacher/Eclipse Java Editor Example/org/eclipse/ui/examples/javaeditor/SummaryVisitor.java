package org.eclipse.ui.examples.javaeditor;

//import java.util.HashSet;
//import java.util.Set;

import org.eclipse.jdt.core.dom.*;
public class SummaryVisitor extends ASTVisitor{
	//Set names = new HashSet();
	int hoverStart;
	int hoverLength;
	int hoverEnd;
	private String lineSummary;
	
	public String getLineSummary(){
		return this.lineSummary;
	}
	public SummaryVisitor(int offset, int hoverLen) {
		this.hoverStart = offset;
		this.hoverLength = hoverLen;
		this.hoverEnd = offset + hoverLen;
	}
	
//	private ASTNode getCurrentNode(){
//		return null;
//	}
	
	private boolean checkBounds(ASTNode node){
		return (this.hoverStart <= node.getStartPosition() + node.getLength()
		&&(this.hoverStart + this.hoverLength >= node.getStartPosition() 
		|| this.hoverStart > node.getStartPosition()));
	}
	public boolean visit(VariableDeclarationFragment node) {
		if(this.checkBounds(node)){
			SimpleName name = node.getName();
			//this.names.add(name.getIdentifier());
			this.lineSummary = "Declaration of '"+ name +"' in line "+name.getStartPosition() + this.lineSummary;
		}
		return false; // do not continue to avoid usage info
	}
	public boolean visit(MethodDeclaration node){
		if (this.checkBounds(node)){
			String Name = node.getName().toString();
			if (!Name.equalsIgnoreCase("main")){
				this.lineSummary = "Declares a method to perform " + Name;
			}
			else{
				this.lineSummary = "Declares the main method for the class";
			}
		}
		return true;
	}

	public boolean visit(MethodInvocation node){
		if (this.checkBounds(node)){
			SimpleName name = node.getName();
			//this.names.add(name.getIdentifier());
			this.lineSummary ="Invocation of '"+ name +"'";
		}
		return false;
	}
	public boolean visit(WhileStatement node){
		if (this.checkBounds(node)){
			Expression e = node.getExpression();
			//this.names.add(node.getBody());
			this.lineSummary = "Execute the following code while '" + e.toString() + "' is true.";
		}
		return true;
	}
	public boolean visit(IfStatement node){
		if (this.checkBounds(node)){
			Expression e = node.getExpression();
			this.lineSummary = "Executes the following code once if '" + e.toString() + "' is true.";
		}
		return true;
	}
	public boolean visit(DoStatement node){
		if (this.checkBounds(node)){
			if (this.checkBounds(node)){
				this.lineSummary = "Executes the following code one time, then checks the condition at the end, and if true, executes the code again.";
			}
		}
		return true;
	}
	public boolean visit(CatchClause node){
		if (this.checkBounds(node)){
			if (node.getException().getType().toString() == "Exception"){
				this.lineSummary = "Executes the following code if an error ('exception') of any kind occus";
			}
			else{
				this.lineSummary = "Executes the following code if an error ('exception') of type " + node.getException().getType().toString() + " occurs";
			}
		}
		return true;
	}
	public boolean visit(BreakStatement node){
		if (this.checkBounds(node)){
			this.lineSummary = "Causes execution of this block of code to cease";
		}
		return false;
	}
	public boolean visit(TryStatement node){
		if (this.checkBounds(node)){
			this.lineSummary = "Execute the following code, with acknowledgement that there may be an error";
		}
		return true;
	}
	public boolean visit(VariableDeclarationStatement node){
		if (this.checkBounds(node)){
			this.lineSummary = "Declare a variable of type " + node.getType().toString();
		}
		return false;
	}
}
