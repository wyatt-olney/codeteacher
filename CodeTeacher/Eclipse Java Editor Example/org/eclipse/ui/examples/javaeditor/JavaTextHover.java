/*******************************************************************************
 * Copyright (c) 2000, 2008 IBM Corporation and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package org.eclipse.ui.examples.javaeditor;


import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Display;

import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IPath;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

import org.eclipse.jface.viewers.IStructuredSelection;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextHover;
import org.eclipse.jface.text.ITextHoverExtension2;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.jface.text.Region;

import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
//import org.eclipse.jdt.core.ITypeRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.core.dom.CompilationUnit;

import SWUM.src.edu.udel.nlpa.swum.utils.identifiers.ConservativeCamelCaseSplitter;

/**
 * Example implementation for an <code>ITextHover</code> which hovers over Java code.
 */
public class JavaTextHover implements ITextHoverExtension2, ITextHover{
	private static ConservativeCamelCaseSplitter splitter = new ConservativeCamelCaseSplitter();
	/**
	 * {@inheritDoc}
	 * 
	 * @deprecated As of 3.4, replaced by
	 *             {@link ITextHoverExtension2#getHoverInfo2(ITextViewer, IRegion)}
	 */
	public Object getHoverInfo2(ITextViewer textViewer, IRegion hoverRegion) {
		String output = ""; //$NON-NLS-1$
		String result;
		String[] outArray;
		int length = 1;
		if (hoverRegion != null) {
			if(hoverRegion.getLength()==0){
				try{
					IDocument doc = textViewer.getDocument();
					int docLen = doc.getLength();
					int offset = hoverRegion.getOffset();
					int begin = offset;
					
//					while ((isNotLineEnd(doc.get(begin,length).charAt(0)))&&(begin>0)){
//						begin--;
//						length++;
//					}
//					while ((isNotLineEnd(doc.get(begin,length).charAt(length-1)))&&(offset+length<docLen)){
//						length++;
//					}
					SummaryVisitor sv = new SummaryVisitor(begin, length);
					CompilationUnit cu = getCompUnit(doc,offset);
					cu.accept(sv);
					result = sv.getLineSummary();
					return result;
				}catch(Exception e){
					return e.toString();
				}
			}
			else if (hoverRegion.getLength() > 0){
				try {
					String highlightText = textViewer.getDocument().
							get(hoverRegion.getOffset(), 
							hoverRegion.getLength());
					outArray = splitter.splitId(highlightText);
					for (int i =0;i<outArray.length;i++){
						output+=(outArray[i]+' ');
					}
					IDocument doc = getIDocument(textViewer);
					final int offset = hoverRegion.getOffset();
					final int hoverLen = hoverRegion.getLength();
					CompilationUnit cu = getCompUnit(doc,offset);
//					AST ast = cu.getAST();
					SummaryVisitor sumVis = new SummaryVisitor(offset,hoverLen);
					cu.accept(sumVis);
					output = sumVis.getLineSummary();
//					String javaElement = getJavaElement(doc, offset);
//					if( javaElement!= null){
//						return output+ javaElement;
//					}
//					else {
						return output;
//					}
				} catch (BadLocationException e) {
					return e.toString();
				}
				//return textViewer.getDocument().get(hoverRegion.getOffset(), hoverRegion.getLength());
//			} catch (BadLocationException x) {
			}else{
				System.err.println("here"); //$NON-NLS-1$
			}
		}
		return null;
	}

	private boolean isNotLineEnd(char charAt) {
		return (charAt != '\n');
	}
	/* (non-Javadoc)
	 * Method declared on ITextHover
	 */
	public IRegion getHoverRegion(ITextViewer textViewer, int offset) {
		Point selection= textViewer.getSelectedRange();
		if (selection.x <= offset && offset < selection.x + selection.y)
			return new Region(selection.x, selection.y);
		return new Region(offset, 0);
	}
	
	public static Display getDisplay() {
	      Display display = Display.getCurrent();
	      //may be null if outside the UI thread
	      if (display == null)
	         display = Display.getDefault();
	      return display;		
	   }
	
	public String getHoverInfo(ITextViewer paramITextViewer, IRegion paramIRegion) {
		return (String)getHoverInfo2(paramITextViewer, paramIRegion);
	}
	public IDocument getIDocument(ITextViewer t){
		return t.getDocument();
	}
	public IFile getIFile(){
		IWorkbenchWindow window=PlatformUI.getWorkbench().getActiveWorkbenchWindow(); 
		try{
		    	if (window != null){
		        IStructuredSelection selection = (IStructuredSelection) window.getSelectionService().getSelection();
		        Object firstElement = selection.getFirstElement();
		        if (firstElement instanceof IAdaptable)
		        {
		            IProject project = (IProject)((IAdaptable)firstElement).getAdapter(IProject.class);
		            IPath path = project.getFullPath();
		            return (IFile)path;
		        }
		        else{
		        	return null;
		        }
		    }else{
		    	return null;
		    }
			
		}catch(Exception e){
			return null; 
		}
	}
	public String getJavaElement(IFile file){
		// Get compilation unit
	    ICompilationUnit unit = JavaCore.createCompilationUnitFrom(file);
	    
	    // Get offset
	    ITextSelection it = (ITextSelection) PlatformUI.getWorkbench().
	        getActiveWorkbenchWindow().getActivePage().getActiveEditor().
	        getEditorSite().getSelectionProvider().getSelection();
	    int offset = it.getOffset();
	    
	    IJavaElement m = null;
	    try {
	        m = unit.getElementAt(offset);
	    } catch (JavaModelException e) {
	    	return e.toString(); 
	    }
	    if (m == null)
	        m = unit;
	    visit(m); // this is where I processed it
	    return m.getElementName();
	}
	
//	public String getJavaElement(IDocument doc, int offset){
//		ASTParser parser = ASTParser.newParser(AST.JLS8);
//		parser.setSource(doc.get().toCharArray());
//		CompilationUnit cu = (CompilationUnit) parser.createAST(null);
//	    IJavaElement m = null;
//	    try {
//	        m = cu.getElementAt(offset);
//	    } catch (JavaModelException e) {
//	    	return e.toString(); 
//	    }
//	    if (m == null)
//	        return m;
//	    return m.getElementName();
//	}
	public CompilationUnit getCompUnit(IDocument doc, int offset){
		 ASTParser parser = ASTParser.newParser(AST.JLS8);
		 parser.setSource(doc.get().toCharArray());
		 CompilationUnit cu = (CompilationUnit) parser.createAST(null);
//		 AST ast = cu.getAST();
		 
		 return cu;
	}
	
	private void visit(IJavaElement m) {
		
	}
	
}